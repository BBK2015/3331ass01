/* Matthew Quigley - 3337679 - COMP3331 Assessment 1
 * 
 * ForumPost holds information about a singular forum post.
 * A database of these posts is stored on both the server
 * side and the client side, and are maintained by a 
 * ForumDatabase object. 
 * 
 */

public class ForumPost {
    String author;			// user who wrote the post
    String bookName;		// the book that the post is related to
    int page;				// the page that the post is related to
    int lineNum;			// the line number that the post is related to
    int id;					// the post ID of the post
    String comment;			// the contents of the post
    boolean read;			// whether or not this post has been read (only relevant to reader, not server)
    
    
    public ForumPost(int lineNum, String comment, int id, String author, int page, String bookName) {
        this.lineNum = lineNum;
        this.comment = comment;
        this.id = id;
        this.author = author;
        this.page = page;
        this.bookName = bookName;
        this.read = false;
    }

    /*
     * 
     * GETTERS
     * 
     */
    public int getId() {
        return id;
    }
    
    public String getBookName(){
        return bookName;
    }    
    
    public String getAuthor(){
        return author;
    }

    public int getPage(){
        return page;
    }
        
    public int getLineNum(){
        return lineNum;
    }
    
    public String getComment() {
        return comment;
    }
    
    public boolean hasBeenRead() {
        return read;
    }
    
    
    /*
     * 
     * SETTERS
     * 
     */
    public void setId(int id) {
        this.id = id;
    }
    
    public void setComment(String comment) {
        this.comment = comment;
    }
    
    public void setRead(boolean read) {
        this.read = read;
    }
}
