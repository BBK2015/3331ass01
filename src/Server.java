/* Matthew Quigley - 3337679 - COMP3331 Assessment 1
 * 
 * Server/Client application for reading eBooks with 
 * a networked user forum.
 * 
 * Server.java serves as the server
 */

import java.io.*;
import java.net.*;
import java.util.*;

public class Server {

    static ForumDatabase forumDatabase;
    static HashMap<String, eBook> books;
    static ArrayList<User> users;
    
    private static int NUM_PAGES = 9;
    
    private static int PULL_MODE = 0;
    private static int PUSH_MODE = 1;
    
    private final static int DISPLAY = 0;
    private final static int POST_TO_FORUM = 1;
    private final static int READ_POST = 2;
    private final static int GET_POST_ID_LIST = 3;
    private final static int GET_POST_BY_ID = 4;
    private final static int REGISTER_USER = 5;
    
    public static void main(String[] args)throws Exception {
        forumDatabase = new ForumDatabase();
        System.out.println("The database for discussion posts has been intialised");
        books = new HashMap<String, eBook>();
        users = new ArrayList<User>();
        initBooks();
        

        int serverPort = Integer.parseInt(args[0]);
        
        // create server socket
        ServerSocket welcomeSocket = new ServerSocket(serverPort);
        System.out.println("The server is now listening on port number: " + serverPort);

        while (true){
            // accept connection from connection queue
            Socket connectionSocket = welcomeSocket.accept();
            // System.out.println("connection from " + connectionSocket);
            
            // create read stream to get input
            BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
            String clientCommand;
            String response = "";
            clientCommand = inFromClient.readLine();
            System.out.println("*************** REQUEST RECEIVED ***************");

            // process input
            if (clientCommand != null){
            	String[] request = clientCommand.split(" ");
                DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());

                switch (Integer.parseInt(request[0])){
                	// add a new user to the user list, push any 
                	// required forum posts to reader if in push mode
                	case REGISTER_USER:
                		String userName = request[1];
                		int pushPort = Integer.parseInt(request[2]);
                		int mode = Integer.parseInt(request[3]);
                		String ip = connectionSocket.getInetAddress().toString();
                		User user = new User(ip, userName, pushPort, mode, -1);    		
                		users.add(user);

                		String modeString;
                		if (mode == PUSH_MODE) modeString = "PUSH MODE";
                		else modeString = "PULL MODE";
                        System.out.println("Registered new user " + userName.toUpperCase() + " operating in " + modeString);

                        if (mode == PUSH_MODE) initialisePushUser(user, welcomeSocket);

                		break;
                	
                    case DISPLAY:
                        System.out.println("User has requested to view " + request[1].toUpperCase() + "(" + request[2] + ")");
                        eBook b = books.get(request[1]);
                        ArrayList<String> page = b.getLines().get(Integer.parseInt(request[2]));
                        
                        // this obviously only works because we know num_pages is always
                        // 9, a real implementation would need real page numbers
                        for (int i = 0; i < NUM_PAGES; i++){
                        	outToClient.writeBytes(page.get(i) + '\n');
                        	System.out.println(page.get(i));
                        }
                        
                        System.out.println(request[1].toUpperCase() + "(" + request[2] + ") has been sent to the user");
                        break;
                        
                    case POST_TO_FORUM:
                    	// receive the post and add it to forumdata base
                    	// loop over the users, checking for those in push mode
                    	// and push the new posts to them
                        String bookName = request[1]; 
                        int pageNumber = Integer.parseInt(request[2]);
                        int lineNumber = Integer.parseInt(request[3]);
                        userName = request[4];
                        String comment = request[5];
                        for (int i = 6; i < request.length; i++) comment = comment + " " + request[i];

                        System.out.println("New post received from: " + userName.toUpperCase());
                        
                        ForumPost post = new ForumPost(lineNumber, comment, forumDatabase.getNumPosts(), userName, pageNumber, bookName);
                        forumDatabase.addPost(post);
                        
                        System.out.println("Post added to the database and given serial number (" + bookName + ", " + pageNumber + ", " + lineNumber + ", " + post.getId() + ").");
                        
                        pushToUsers(post);

                        break;
                        
                    case GET_POST_ID_LIST:
                    	// return a list of forum post ID's to the client
                        System.out.println("User has requested a list of forum posts related to " + request[1].toUpperCase() + "( " + request[2] + ")");

                        String postList = forumDatabase.getPostIdList(request[1], Integer.parseInt(request[2]));
                        outToClient.writeBytes(postList + '\n');
                        if (postList.equals("")){
                            System.out.println("No posts to send back");
                        } else {
                            System.out.println("Sending back ID's of posts related to requested book and page. List = " + postList);
                        }
                        break;
                        
                    case GET_POST_BY_ID:
                    	// provide the client with a requested forum post
                    	// given by its serial ID
                        System.out.println("User has requested details of forum post with serial id " + request[1]);
                        ForumPost fp = forumDatabase.getPostByID(Integer.parseInt(request[1]));
                        response = "";
                        if (fp != null) response = fp.getLineNum() + " " + fp.getPage() + " " + fp.getId() + " " + fp.getAuthor() + " " + fp.getBookName() + " " + fp.getComment();
                        outToClient.writeBytes(response + '\n');
                        System.out.println("Forwarded forum post " + request[1] + " to the user");
                        break;
                        
                    default:
                        break;
                }
            }
            
            System.out.println("*************** REQUEST FULFILLED ***************");
        } // end of while (true)

    } // end of main()

	/**
     * This function is used when a new user connects who is in push mode.
     * This function first creates a persistent connection with that client
     * and stores that socket away. After receiving a list of posts that 
     * the client has, the server will reply with any posts the client does not have.
     *    
     * @param u the user being initialized
     * @param welcomeSocket the welcomeSocket of the server, used to create a new connection to the server		
     * @return void     
     */
    private static void initialisePushUser(User u, ServerSocket welcomeSocket) throws IOException {
        Socket pushSocket = welcomeSocket.accept();
		u.setSocket(pushSocket);
        DataOutputStream pushStream = new DataOutputStream(pushSocket.getOutputStream());

		// first let the client know how many replies from the server to expect (can be 0)
		String numPosts = Integer.toString(forumDatabase.getNumPosts());
		pushStream.writeBytes(numPosts + '\n');
        System.out.println("Preparing to forward " + numPosts + " posts to " + u.getUserName().toUpperCase());
		// then loop through, looking for the required posts, and send one by one
        for (ForumPost fp : forumDatabase.getPosts()){
            String response = fp.getLineNum() + " " + fp.getPage() + " " + fp.getId() + " " + fp.getAuthor() + " " + fp.getBookName() + " " + fp.getComment();
            pushStream.writeBytes(response + '\n');
            System.out.println("Forwarded forum post " + fp.getId() + " to " + u.getUserName().toUpperCase());
		}
		
	}

	/**
     * Pushes a newly received forum post to all users
     * in push mode. Does so by iterating over users in push
     * mode, grabbing the persistent socket that was set up
     * when user connected to server, and then pushing the 
     * post to the users.        
     *      
     * @param 	p forumPost to be forwarded	
     * @return  void    
     */
	private static void pushToUsers(ForumPost p) throws IOException {
        int numUsersInPush = 0;
        for (User u : users){
        	if (u.getMode() == PUSH_MODE){
        		DataOutputStream pushStream = new DataOutputStream(u.getSocket().getOutputStream());
                String response = p.getLineNum() + " " +  p.getPage() + " " + p.getId()
                			+ " " +  p.getAuthor() + " " + p.getBookName() + " " + p.getComment();
                pushStream.writeBytes(response + '\n');
                numUsersInPush++;
        	}
        }
        if (numUsersInPush == 0) System.out.println("No users operating in push mode: No action required" );
        else System.out.println("Pushed new post to " + numUsersInPush + " users in push mode" );
	}


	/**
     * Initializes the .txt file books in the given folder.
     * Loops over given folder, creating a new book if needed
     * depending on the file name, adding it to the hash map
     * of books, and populating its pages.        
     *      
     * @return    void  
     */
    public static void initBooks() throws FileNotFoundException, IOException{
        File folder = new File("../src/pages/");
        File[] listOfFiles = folder.listFiles();
        for (File file : listOfFiles) {
            eBook book;
            if (file.isFile()) {
                String author = file.getName().split("_")[0];
                if (books.containsKey(author)){
                    book = books.get(author);
                } else {
                    int id = books.size();
                    book = new eBook(author, id);
                    books.put(author, book);
                }
                
                int page = Integer.parseInt(file.getName().split("_")[1].split("page")[1]);               
                try (BufferedReader br = new BufferedReader(new FileReader(file))) {
                    String line;
                    while ((line = br.readLine()) != null) {
                       book.addLine(page, line);
                    }
                }
            }
        }
        System.out.println("eBooks have been populated and database initialised");
    }
    
    
} // end of class TCPServer