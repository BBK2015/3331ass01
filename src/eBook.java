/* Matthew Quigley - 3337679 - COMP3331 Assessment 1
 * 
 * An eBook is used to store all the required information about 
 * a given book. 
 * 
 */

import java.util.*;


public class eBook {
    int id;										// integer ID of the book
    String author;								// author of the book/book name
    HashMap<Integer, ArrayList<String>> pageLines;		// content of the pages

    
    public eBook(String author, int id) {
        this.author = author;
        this.id = id;
        pageLines = new HashMap<Integer, ArrayList<String>>();
    }

    /**
     * Prints a book to the screen (all pages/lines)        
     */
    public void printBook(){
        System.out.println("Author: " + author + ", ID: " + id);
        for (int i = 0; i < pageLines.size(); i++){
            ArrayList<String> page = pageLines.get(i);
            for (int j = 0; j < page.size(); j++){
                System.out.println(page.get(j));
            }
        }
    }
    
    /**
     * Prints a given page to the screen       
     *      
     * @param 	pageNumber the page to be printed	
     */
    public void printPage(int pageNumber){
        System.out.println("Author: " + author + ", ID: " + id + ", Page: " + pageNumber);
        ArrayList<String> page = pageLines.get(pageNumber);
        for (int j = 0; j < page.size(); j++){
            System.out.println(page.get(j));
        }
    }
    
    /**
     * Adds a line to the book at the given page number       
     *      
     * @param pageNumber the number to add the line
     * @param line the line to be added		
     */
    public void addLine(int pageNumber, String line) {
    	if (pageLines.containsKey(pageNumber)){
            pageLines.get(pageNumber).add(line);  
        } else {
        	ArrayList<String> newPage = new ArrayList<String>();
        	newPage.add(line);
        	pageLines.put(pageNumber, newPage);
        }
    }
    
    /*
     * 
     * GETTERS AND SETTERS
     * 
     */
    public String getAuthor(){
        return author;
    }
    
    public HashMap<Integer, ArrayList<String>> getLines() {
        return pageLines;
    }

    public void setLines(HashMap<Integer, ArrayList<String>> lines) {
        this.pageLines = lines;
    }
}
