/* Matthew Quigley - 3337679 - COMP3331 Assessment 1
 * 
 * ForumDatabase maintains a hash map(serialID, forumPost) database of 
 * forum posts.
 * 
 */

import java.util.*;

public class ForumDatabase {
    HashMap<Integer, ForumPost> forumPosts;
    
    public ForumDatabase(){
        this.forumPosts = new HashMap<Integer, ForumPost>();
    }
    

    public void addPost(ForumPost fp){
        forumPosts.put(fp.getId(), fp);
    }
    
   public ForumPost getPostByID(int id){  
        return forumPosts.get(id);
    }
    
    public int getNumPosts(){
        return forumPosts.keySet().size();
    }   
    
    public boolean exists(int id){
        return forumPosts.containsKey(id);
    }
    
    public boolean isEmpty(){
    	return forumPosts.isEmpty();
    }
    
    
    /**
     * Returns a list of posts that are relevant
     * to a given book and page.        
     *      
     * @param 	bookName name of the desired book
     * @param 	pageNumber page number desired	
     * @return  p an ArrayList of ForumPosts related to bookName and pageNumber    
     */
    public ArrayList<ForumPost> getPosts(String bookName, int pageNumber){
        ArrayList<ForumPost> p = new ArrayList<ForumPost>();
        for(ForumPost fp: forumPosts.values()){
            if (fp.getBookName().equals(bookName) && (fp.getPage() == pageNumber)){
                p.add(fp);
            }
        }
        return p;
    }
    
    /**
     * Returns every post in the database.       
     *      
     * @return   p an ArrayList of every forum post in the database   
     */
    public ArrayList<ForumPost> getPosts(){
        ArrayList<ForumPost> p = new ArrayList<ForumPost>();
        for(ForumPost fp: forumPosts.values()){
        	p.add(fp);
        }
        return p;    
    }
    
    /**
     *        
     *      
     * @param 		
     * @return      
     */
    public ArrayList<ForumPost> getLinePosts(String bookName, int pageNumber, int lineNumber){
        ArrayList<ForumPost> p = new ArrayList<ForumPost>();
        for(ForumPost fp: forumPosts.values()){
            if (fp.getBookName().equals(bookName) && (fp.getPage() == pageNumber) && (fp.getLineNum() == lineNumber)){
                p.add(fp);
            }
        }
        return p;
    }
    
    /**
     * Returns a list of keys to the hash map.
     * This is a list of serial IDs contained within 
     * the database related to the given book and page.       
     *      
     * @param 	bookName the name of the desired book
     * @param 	page the desired page number	
     * @return  posts a string of post ID's    
     */
    public String getPostIdList(String bookName, int page){
        String posts = "";
        Set<Integer> keys = forumPosts.keySet();
        for (Integer key : keys){
            ForumPost fp = forumPosts.get(key);
            if (fp.getBookName().equals(bookName) && fp.getPage() == page){
                posts = fp.getId() + " " + posts;
            }
        }
        return posts;
    }

    /**
     * Prints the information contained within the database         
     */
    public void print(){
        Set<Integer> keys = forumPosts.keySet();
        for (Integer key : keys){
            ForumPost fp = forumPosts.get(key);
            System.out.println(fp.getId() + " " + fp.getAuthor() + " " + fp.getLineNum() + " " + fp.getComment());
        }
    }
}
