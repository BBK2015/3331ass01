/* Matthew Quigley - 3337679 - COMP3331 Assessment 1
 * 
 * Server/Client application for reading eBooks with 
 * a networked user forum.
 * 
 * Reader.java serves as the client
 */

import java.io.*;
import java.net.*;
import java.util.*;


public class Reader {
    private static int NUM_PAGES = 9;
    
    private static int PULL_MODE = 0;
    private static int PUSH_MODE = 1;
    
    private final static int DISPLAY = 0;
    private final static int POST_TO_FORUM = 1;
	private final static int READ_POST = 2;
    private final static int GET_POST_ID_LIST = 3;
    private final static int GET_POST_BY_ID = 4;
    private final static int REGISTER_USER = 5;

    static ForumDatabase forumDatabase;
    static User user;
    static Timer timer;
    static InetAddress serverIPAddress;
    static int serverPort;
    static Socket pushSocket;
    
    static boolean newPostsFlag;

    public static void main(String[] args) throws Exception {
       forumDatabase = new ForumDatabase();

       int mode;
       if (args[0].equals("pull")) mode = PULL_MODE;
       else mode = PUSH_MODE;
       int pollingInterval = Integer.parseInt(args[1]);
       String userName = args[2];
       serverIPAddress = InetAddress.getByName(args[3]);
       serverPort = Integer.parseInt(args[4]);
       newPostsFlag = false; 
       
       user = new User("", userName, serverPort, mode, pollingInterval);
       registerUserWithServer();
       
       // set up a push stream for the server to push
       // new forum posts on (only used in push mode)
       BufferedReader pushStream = null;
       if (mode == PUSH_MODE) pushStream = 
    		   new BufferedReader(new InputStreamReader(pushSocket.getInputStream()));

       while (true){  
    	   // if we are in push mode, and the input stream being used for push messages
    	   // is ready, read it and add the forum post to the database
    	   if (mode == PUSH_MODE && pushStream.ready()){
    		   receiveForumPost(pushStream.readLine());
    	   }

           // get input from keyboard
           BufferedReader inFromUser =
               new BufferedReader(new InputStreamReader(System.in));
      
           // if the user hasn't given input, we continue 
           // the while loop, this allows us to keep checking for
           // newly pushed forum posts
           if (!inFromUser.ready()) continue;
           newPostsFlag = false;
           String sentence = inFromUser.readLine();
           String[] arguments = sentence.split(" ");
           String command = arguments[0];
           
           // create socket which connects to server, as well as an input and output stream
           Socket clientSocket = new Socket(serverIPAddress, serverPort);
           DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
           BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
           
           String toSend;
           
           switch(command){
           	   case "display":
					user.setCurrentBook(arguments[1]);
					user.setCurrentPage(Integer.parseInt(arguments[2]));
					
					toSend = DISPLAY + " " + arguments[1] + " " + arguments[2]; 
					outToServer.writeBytes(toSend + '\n');
					
					// obviously this only works because we 
					// know each book contains only 9 pages
					String[] pageLines = new String[NUM_PAGES];
					for (int i = 0; i < NUM_PAGES; i++){
						String line = inFromServer.readLine();
						pageLines[i] = line;
					}
					
					// if in pull mode, pull any new forum posts
					// and create a timer to automatically check
					// for new posts after the given polling period
					if (user.getMode() == PULL_MODE){ 
						refreshForumPosts(serverIPAddress, serverPort);			
						setTimer();               								
					} 
					
					printPage(pageLines);
           		    break;
           		    
           	   case "post_to_forum":
                   int lineNumber = Integer.parseInt(arguments[1]);
                   String comment = arguments[2];
                   for (int i = 3; i < arguments.length; i++) comment += " " + arguments[i];
                   
                   // toSend = 1 currentBook lineNumber userName comment
                   toSend = POST_TO_FORUM + " " + user.getCurrentBook() + " " + user.getCurrentPage() + " " +  lineNumber + " " + user.getUserName() + " " + comment; 
                   outToServer.writeBytes(toSend + '\n');
           		   
           		   break;
           		   
           	   case "read_post":
                   clientSocket.close();			// close the socket to unblock the server so query from refreshForumPosts() is served
           		   refreshForumPosts(serverIPAddress, serverPort);	// make sure we have the most up to date posts
           		   
                   lineNumber = Integer.parseInt(arguments[1]);
                   // get all posts related to the given book, page and line
                   ArrayList<ForumPost> p = forumDatabase.getLinePosts(user.getCurrentBook(), user.getCurrentPage(), lineNumber);
                   int page = user.getCurrentPage() + 1;
                   // print the heading
                   System.out.println("Book by: "+ user.getCurrentBook() + ", Page " + page + ", Line number " + lineNumber + ":");
                   // loop through those pages, if any given post hasn't been read, print it and mark as read
                   
                   for (ForumPost fp : p){
                       if (!fp.hasBeenRead()){
                           System.out.println(fp.getId() + " " + fp.getAuthor() + ": " + fp.getComment());
                           fp.setRead(true);
                       }
                   }
           		   
           		   break;
           		   
       		   default: 
       			   break;
           }
           clientSocket.close();
       }
   } // end of main


    /**
     * Prints a page of a book, first checks whether there are any new
     * posts related to this page (checks from local forum post DB)
     * and modifies the lines with 'n' or 'm' accordingly, then prints
     * the line to the screen.       
     *      
     * @param 		pageLines the lines of the page to print		
     * @return      void
     */
	private static void printPage(String[] pageLines) {
		ArrayList<ForumPost> p = forumDatabase.getPosts(user.getCurrentBook(), user.getCurrentPage());
		for (ForumPost fp : p){
			// use a string builder to replace the 2nd character to n or m
		    StringBuilder line = new StringBuilder(pageLines[fp.getLineNum()-1]);
		    
		    if (fp.hasBeenRead()) line.setCharAt(1, 'm');
		    else line.setCharAt(1, 'n');
		    
		    pageLines[fp.getLineNum()-1] = line.toString();
		}

		// print output
		for (int i = 0; i < pageLines.length; i++) System.out.println(pageLines[i]);		
	}

    /**
     * Registers the user with the server. Provides the server with
     * information such as username, ip and port that the reader
     * is listening on for pushed forum posts.      
     *      
     * @return      void
     */
	private static void registerUserWithServer() throws IOException {
		// create a socket and an output stream
        Socket clientSocket = new Socket(serverIPAddress, serverPort);
        DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
        
        String toSend = REGISTER_USER + " " + user.getUserName() + " " + serverPort + " " + user.getMode();
        outToServer.writeBytes(toSend + '\n');

        // if in push mode, set up a socket which listens for pushed forum posts
        // this socket is persistent and does not close down for the duration of running
        if (user.getMode() == (PUSH_MODE)){
            pushSocket = new Socket(serverIPAddress, serverPort);
            BufferedReader inFromServer = new BufferedReader(new InputStreamReader(pushSocket.getInputStream()));
            // server will reply first with a message containing how many
            // forum posts it is about to push
            String fromServer = inFromServer.readLine();
            String[] args = fromServer.split(" ");
            int numNewPosts = 0;
            if (args.length == 1) numNewPosts = Integer.parseInt(args[0]);
            
            // we now know how many times to loop as we 
            // receive the required amount of forum posts
        	for (int i = 0; i < numNewPosts; i++){
                fromServer = inFromServer.readLine();
            	receiveForumPost(fromServer);
        	}
        }
        clientSocket.close();
	}


    /**
     * Adds a new forum post to the local database
     * after it is received from the server. Prints
     * to screen if the new post is related
     * to the currently viewed book and page         
     *      
     * @param 		post the string received from the server containing the forum post
     * @return      void
     */
	private static void receiveForumPost(String post) {
		String[] fpArgs = post.split(" ");
		
		int lineNumber = Integer.parseInt(fpArgs[0]);
        int pageNumber = Integer.parseInt(fpArgs[1]);
        int id = Integer.parseInt(fpArgs[2]);
        String userName = fpArgs[3];
        String bookName = fpArgs[4];
        String comment = fpArgs[5];
        for (int j = 6; j < fpArgs.length; j++) comment = comment + " " + fpArgs[j];
       
        ForumPost fp = new ForumPost(lineNumber, comment, id, userName, pageNumber, bookName);
        forumDatabase.addPost(fp);
        
        // if the forum post we received is relevant to current book 
        // and page being viewed, let the user know
        if (fp.getBookName().equals(user.getCurrentBook()) && fp.getPage() == user.getCurrentPage()){
        	if (!newPostsFlag) System.out.println("There are new posts.");
            newPostsFlag = true;
        }
	}

    /**
     * If the user is in pull mode, this function is called to first
     * request a list of forum post ID's from the server that are related
     * to the currently viewed book and page. It then compares it to the list
     * of ID's in the readers local database. Any missing forum posts are the requested
     * from the server and added to the local DB.       
     *      
     * @param 	   serverIPAddress	the IP address of the server
     * @param 	   serverPort		the port that the server is listening on
     * @return     void 
     */
    private static void refreshForumPosts(InetAddress serverIPAddress, int serverPort) throws IOException{
        Socket listRequestSocket = new Socket(serverIPAddress, serverPort);
        DataOutputStream listRequestOut = new DataOutputStream(listRequestSocket.getOutputStream());

        // request a list of ID's from the server
        String toSend = Integer.toString(GET_POST_ID_LIST) + " " + user.getCurrentBook() + " " + user.getCurrentPage();
        listRequestOut.writeBytes(toSend + '\n');
        
        BufferedReader listRequestIn = new BufferedReader(new InputStreamReader(listRequestSocket.getInputStream()));
        // wait for list to be returned from the server
        String fromServer = listRequestIn.readLine();

        // if there are IDs in the list that was returned
        if (!fromServer.equals("")){
            String[] newPostList = fromServer.split(" ");
            
            // iterate through the provided ID's, checking
            // if they exist in the local database of forum posts.
            // if they do not, add them
            // every time we find a forum post in the server database
            // that is not in our own, we open a new socket and use
            // a post request command to retrieve a single post
            // before closing the socket and starting again.
            // another approach would be to establish how many posts would
            // be required and synchronize the reader and server accordingly
            for (int i = 0; i < newPostList.length; i++){
                if (!forumDatabase.exists(Integer.parseInt(newPostList[i]))){
                    toSend = GET_POST_BY_ID + " " + newPostList[i];
                    
                    Socket postRequestSocket = new Socket(serverIPAddress, serverPort);
                    DataOutputStream postRequestOut = new DataOutputStream(postRequestSocket.getOutputStream());
                    postRequestOut.writeBytes(toSend + '\n');
                    
                    BufferedReader postRequestIn = new BufferedReader(new InputStreamReader(postRequestSocket.getInputStream()));
                    String serverReply = postRequestIn.readLine();
                    receiveForumPost(serverReply);

                    postRequestSocket.close();
                }
            } 
        } 
        
        listRequestSocket.close();
    }
    
    /**
     * Sets a timer to call for new forum posts after an interval 
     * given in command line arguments.       
     *      
     * @return   void   
     */
	private static void setTimer() {
        if (timer != null) timer.cancel();
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                    try {
                    	//newPostsFlag = false;
                    	refreshForumPosts(serverIPAddress, serverPort);
					} catch (IOException e) {
						e.printStackTrace();
					}
            }
        }, 1000 * user.getPollingInterval(), 1000 * user.getPollingInterval());
    }
}