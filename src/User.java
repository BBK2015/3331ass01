/* Matthew Quigley - 3337679 - COMP3331 Assessment 1
 * 
 * User holds information relevant to the user
 * who using a Reader. It is used by both Reader and
 * Server to store information, but not all fields are
 * needed for both users, i.e polling interval is only
 * required by Reader.java. 
 */

import java.net.*;

public class User {

    String userName;		// username of user
    int mode;				// pull or push
    String ip;				// ip address identifier for user
    int port;				// listening port of user
    Socket socket;			// server uses this field to track the sockets of users in push mode
    int pollingInterval;	// polling interval for pull mode
    String currentBook;		// the book currently viewed by the user
    int currentPage;		// the page currently viewed by the user
    
    public User(String ip, String userName, int port, int mode, int pollingInterval) {
        this.ip = ip;
    	this.port = port;
        this.userName = userName;
        this.mode = mode;
        this.pollingInterval = pollingInterval;
        this.currentBook = "";
        this.currentPage = 0;
    }


    /*
     * 
     * GETTERS AND SETTERS
     * 
     */
    
    public int getPollingInterval() {
		return pollingInterval;
	}

	public void setPollingInterval(int pollingInterval) {
		this.pollingInterval = pollingInterval;
	}

	public String getCurrentBook() {
		return currentBook;
	}

	public void setCurrentBook(String currentBook) {
		this.currentBook = currentBook;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public Socket getSocket() {
        return socket;
    }
    
    public void setSocket(Socket s) {
        socket = s;
    }
    
    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    } 
}
